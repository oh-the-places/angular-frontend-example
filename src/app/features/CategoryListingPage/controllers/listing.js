function ListingController($scope, products) {
    $scope.name = 'Truefacet Category';
    $scope.hasSpecialPrice = false;
    $scope.products = products;
}

export default ListingController;