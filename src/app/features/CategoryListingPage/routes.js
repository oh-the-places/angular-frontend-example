routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
    $stateProvider
        .state('clp', {
            url: '/clp',
            template: require('./views/listing.html'),
            controller: 'ListingController',
            controllerAs: 'ListingController',
            resolve: {
                products: function($stateParams) {
                    return fetch('http://truefacet-api.com/api/products/search?categoryId%5B0%5D='+12)
                        .then(function(response) {
                            return response.json();
                        })
                        .then(function(data) {
                            if(data.data.hits.hits.length){
                                return data.data.hits.hits
                            }

                        });
                }
            }
        });
}