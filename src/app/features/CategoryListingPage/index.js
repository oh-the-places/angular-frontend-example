import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './routes';
import ListingController from './controllers/listing';

export default angular.module('app.CategoryListingPage', [uirouter])
    .config(routing)
    .controller('ListingController', ListingController)
    .name;