'use strict';

var angular = require('angular');

export default angular.module('app', [])
    .service('CategoriesService', require('./categories'))
    .name;