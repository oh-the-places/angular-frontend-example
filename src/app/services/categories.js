'use strict';

CategoriesService.$inject = ['$http'];

function CategoriesService($http) {
    var _this = this;
    _this.getStatus = function getProducts(catId) {
        return $http({
            url: 'http://truefacet-api.com/api/products/search?categoryId%5B0%5D='+catId
        });
    }
}

module.exports = CategoriesService;