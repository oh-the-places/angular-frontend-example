import angular from 'angular';

// Imported style, handled by webpack. See webpack.config.js
import '../../styles/index.scss';

import routing from './config';
import uirouter from 'angular-ui-router';

require('./services/index');
/**
 * Features
 */
import CategoryListingPage from './features/CategoryListingPage/index';

let app = () => {
  return {
    template: require('./app.html'),
    controller: 'AppCtrl',
    controllerAs: 'app'
  }
};



class AppCtrl {
  constructor() {
    this.url = 'https://www.truefacet.com';
  }
}

const MODULE_NAME = 'app';




// Imported features
const modules = [
  'ui.router',
  CategoryListingPage
];

angular.module(MODULE_NAME, modules)
    .config(routing)
    .controller('AppCtrl', AppCtrl)
    .directive(MODULE_NAME, app);

export default MODULE_NAME;